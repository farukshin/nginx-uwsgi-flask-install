sudo apt-get update
sudo apt-get -y install python3-pip nginx git uwsgi uwsgi-plugin-python3

sudo -H pip3 install --upgrade pip
sudo pip3 install uwsgi flask

python3 --version
pip3 --version
nginx -V
uwsgi --version

sudo service nginx stop
sudo service uwsgi stop
sudo rm /etc/nginx/sites-enabled/default
sudo rm /etc/nginx/sites-available/default

git clone https://gitlab.com/farukshin/nginx-uwsgi-flask-install.git
sudo cp /home/farukshin_amir/nginx-uwsgi-flask-install/nginx-conf.conf /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/nginx-conf.conf /etc/nginx/sites-enabled/
sudo nginx -t


sudo cp /home/farukshin_amir/nginx-uwsgi-flask-install/uwsgi-conf.ini /etc/uwsgi/apps-available/
sudo ln -s /etc/uwsgi/apps-available/uwsgi-conf.ini /etc/uwsgi/apps-enabled/

git clone https://gitlab.com/farukshin/deuteron.git
sudo pip3 install -r requirements.txt

sudo service uwsgi start && sudo service nginx start

